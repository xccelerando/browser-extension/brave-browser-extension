# Latest extension architecture / near term plan

2020/03/26

Here is the current architecture of the extension & its backend for the first implementation

<img src="../schemas/full_architecture_ver2.png" width="600">

## (1) Extension

The extension:

1. Registers / logins a user to the user database owned by XDO (see [issue](https://gitlab.com/xccelerando/browser-extension/brave-browser-extension/-/issues/1));
1. Collects individual behaviors (see [issue](https://gitlab.com/xccelerando/browser-extension/brave-browser-extension/-/issues/3));
1. Records raw behaviors to the data-store (see [repo](https://gitlab.com/xccelerando/browser-extension/data-storage))
2. Sends raw behaviors to Aigents, which does conversion from raw behavior data to AL (see [issue](https://github.com/aigents/aigents-java/issues/11) on [aigents-java](https://github.com/aigents/aigents-java/) repo, implemented by Anton); 
4. Aigents will do the content search and delivery;

Distribution of modules:

* The extension UI (1) will of course run on the client browser. For all wireframes, see [UI/UX design issue](https://gitlab.com/xccelerando/browser-extension/brave-browser-extension/-/issues/6);
* Settings database (3) will be hosted and owned by XDO forever (or at least until we find a way to implement distributed identifiers and all goes to the blockchain/whatever -- not gonna happen soon, but worth to keep an eye on that);
* Database of behaviors (2) in the first implementation will be hosted by XDO, but should be implemented as a module that could be hosted anywhere as per user's preferences. The address and credentials of database will be specified in extension settings. It should be possible to run this database on user premises too (e.g. the same computer that runs the browser);
* Data conversion and content search/delivery for now will be done by monolithic Aigents server (probably  hosted on XDO). 

## (2) Database of behaviors

See [repo](https://gitlab.com/xccelerando/browser-extension/data-storage);

## (3) Settings database

All login, user profile and extension settings shall be recorded into user database owned by XDO. See [issue](https://gitlab.com/xccelerando/browser-extension/brave-browser-extension/-/issues/1) on login system; 

## (4) Raw data conversion / profiling / keyword extraction

As of current implementation, the raw data conversion to AL statements and recording these statements to Aigents rule database will be done on a single Aigents server, probably hosted on XDA premises. Nevertheless, the aim is to break this into a separate module -- within the first iteration if time permits. If time does not permit, we at least have clear understanding of data formats involved;
* [**important**] This includes recording the AL statement to which the raw behavior was converted into the database of behaviors; For now Aigents should do that, but in the future it will be done by separate module;

## (5) Content search and delivery

Aigents backend will:

* [**critical**] produce user-based RSS feed based on the topics which will then be read and displayed by the extension (see [issue](https://gitlab.com/xccelerando/browser-extension/brave-browser-extension/-/issues/2));
* [**important**] Record the news found per each rule into the database of behaviors;

## Data exchange

TBD: data formats, input / output, etc.:

* (**1-2,4**): Individual behaviors captured by the browser;
* (**1-3**): Login and settings information;
* (**4-5**): AL statements;
* (**5-1**): Content delivery;
* (**4-1**): Patterns extracted from user behaviors;
* (**5-1**): News items extracted per each rule;

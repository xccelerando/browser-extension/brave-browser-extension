Functionality of browser plugin (by modules):

1. UI: The browser plugin collects the information (indicated by Anton):

	1. Get search texts;
	2. Get clicks on texts;
	3. Get selection of text;
	5. Allow to highlight and categorize texts (i.e. via https://web.hypothes.is/ plugin or an in-house developed solution);

A user can have different profiles which may (or may not) be related between each other. These profiles collect different information (e.g. work related / project related, personal interests, etc.)

Ideally, it should be possible to integrate all information about user's interests (e.g. collect also highlights on the pdf documents that a person is reading / highlighting locally -- see https://bitbucket.org/vveitas/yspaceship/src/master/)

2. Database: The information is stored on the device or securely somewhere; A user can delete one or another profile him/herself without any need for third party;

3. Model learning: A model of user's interests is learned from this data on the user's device or via SNET interface. Most probably, in order to best learn user interests model, one needs to pool data from all users; but, the identities of users can be protected. This is a topic of feredated learning, but 

4. Connection to aigents: A model produces whatever representation is needed for agents to construct personalized news feeds. Btw, can we run aigents.jar on the mobile? The jar is 14Mb and java runs on mobile. Yet I believe aigents itself does not learn a model, but uses structured AL expressions to record user's preferences, where these preferences have to be manually encoded.

The module 3 could be implemented either with one or few intermediate steps (e.g. training a model in Sergey's lab, exposing it for inference via SNET interface, etc.).
1. How do aigents learn topics and how machine learning works? So far I saw only how to manually encode patterns, but how do we get them automatically / implicitly?
2. It seems that any personalization would need user-ased aigents integration, but the current direction is channel-based integration. How are we going to go about this?
3. Anything done into the direction of 'user-based integration requirements definition' -- i see that the specification is done -- how can I get it?
4. Profile mining -- how does it work?
5. So there is a social network via aigents after all? but then how will it work 
6. Content highlighting based on word/term/pattern relevance - TODO (May-Jun) -- can we reuse the yspaceship for this?
7. In the plan there is '2.21. Browser plugin - TODO (Q1 2022)' -- anything was thought about this more?
8. Any plans to integrate a webcrawler, to get news / relevant items from the websites 
9. How heavy is Aigents server / is there a point to set up one for each user (?)
10. Do we have 'dimensions' of reputation, or reputation is calculated only for a user?  If so, there is no distinction on what topics a user votes or connects.

Data that can be collected from browser plugin:
1. Get search texts (feed big brother)
2. Get clicks on texts (feed big brother)
3. Get selection of text (feed big brother)
4. Get copypastes of texts (feed big brother)
5. Render highlighting of texts (let big brother helping you to see what you are supposed to see)
6. Filter content (let big brother helping you to avoid seeing what you are not supposed to see)   
7. Get mouse hovers and times the texts are opened (that has been discussed with @Ben Goertzel and @Ibby earlier but I trust this data 10 times less than to anything listed above) 